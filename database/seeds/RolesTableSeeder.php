<?php

use Illuminate\Database\Seeder;
use App\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $roles = ['Admin', 'Manager', 'Analyst'];

        foreach($roles as $role_name){
            Role::create([
                'role_name' => $role_name
            ]);
        }

    }
}
