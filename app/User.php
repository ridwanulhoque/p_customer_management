<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

     protected $guarded = ['id'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public static function boot(){
        parent::boot();

        if(! app()->runningInConsole()){
            static::creating(function($model){
                $model->fill([
                    'contact_number' => '01711223344'
                ]);
            });
        }
    }

    public function user_role(){
        return $this->hasOne(UserRole::class);
    }

    public function admin(){
        return $this->user_role()->where('role_id', 1);
    }

    public function manager(){
        return $this->user_role()->where('role_id', 2);
    }

    public function analyst(){
        return $this->user_role()->where('role_id', 3);
    }
}
