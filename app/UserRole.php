<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserRole extends Model
{
    protected $guarded = ['id'];
    
    public function user(){
        $this->belongsTo(App\User::class);
    }


    public function role(){
        $this->belongsTo(App\Role::class);
    }
}
