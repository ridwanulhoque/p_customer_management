<?php

namespace App\Http\Controllers;

use App\Customer;
use Illuminate\Http\Request;
use App\Http\Requests\CustomerUpdateRequest;

class CustomerController extends Controller
{


    public function __construct()
    {
        // parent::__construct();

        // $this->middleware('analyst', ['only' => 'index']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customers = Customer::orderByDesc('id')->paginate(10);
        return \view('customers.index', compact('customers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('customers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CustomerCreateRequest $request)
    {
        
        $customer = Customer::create([
            'name' => $request->full_name,
            'contact_number' => $request->contact_number,
            'address' => $request->address,
            'attatchment' => $request->attatchment
        ]);

        if($customer){
            return redirect('customers')->with(['message' => 'success']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function show(Customer $customer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function edit(Customer $customer)
    {
        return \view('customers.edit', compact('customer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function update(CustomerUpdateRequest $request, Customer $customer)
    {
        $customer = Customer::where('id', $customer->id)->update([
            'name' => $request->full_name,
            'address' => $request->address,
            'contact_number' => $request->contact_number,
            'attachment' => $request->attatchment
        ]);


        if($customer){
            return \redirect('customers')->with(['message' => 'Update Success!']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Customer $customer)
    {
        $customer_delete = $customer->destroy();
        if($customer_delete){
            return redirect('customers')->with(['message' => 'Delete success!']);
        }
    }
}
