@extends('layouts.app')

@section('content')

<div class="wrapper">
        <div class="row">
            <div class="col-md-12 content-justify-center">
                <div class="card">
                    <div class="card-header">
                        User List
                    </div>
                    <div class="card-body">
                        
                        @if(Session::get('message'))
                        <div class="alert alert-success">
                            {{ Session::get('message')  }}
                        </div>
                        @endif

                        <table class="table table-bordered">
                            <tr>
                                <th>Name</th>
                                <th>Email Address</th>
                                <th>Contact Number</th>
                                <th>Edit</th>
                                <th>Delete</th>
                            </tr>
                            @foreach($users as $user)    
                            <tr>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->email }}</td>
                                <td>{{ $user->contact_number }}</td>
                                <td><a href="{{ route('users.edit', $user->id) }}">Edit</a></td>
                                <td><a href="{{ route('users.destroy', $user->id) }}" onclick="return confirm('sure ?')" >Delete</a></td>
                            </tr>
                            @endforeach
                        </table>

                    </div>
                </div>
            </div>
        </div>
</div>


@endsection