@extends('layouts.app')

@section('content')
<div class="wrapper">
    <div class="row">
        <div class="col-md-12 content-justify-center">
            <div class="card">
                <div class="card-header">
                    Create New User
                </div>
                <div class="card-body">


                    @if($errors->any())
                        <ul>
                            @foreach($errors->all() as $error)
                                <li class="text-danger">
                                    {{ $error }}
                                </li>
                            @endforeach
                        </ul>

                    @endif


                        <form action="{{ route('users.store') }}" method="POST">
                                {{-- <input type="hidden" name="csrf_token" id="csrf_token" value="{{ csrf_token() }}"> --}}
                                
                                @csrf

                                <label for="">Name</label>
                                <div class="form-control">
                                    <input type="text" name="full_name" id="full_name" class="form-control" placeholder="Ex: Mr. X">
                                </div>
                                <label for="">Username/Email</label>
                                <div class="form-control">
                                    <input type="email" name="email_address" id="email_address" class="form-control" placeholder="Ex: user@gmail.com">
                                </div>
                                <label for="">Mobile Number</label>
                                <div class="form-control">
                                    <input type="number" maxlength="10" name="contact_number" id="contact_number" class="form-control" placeholder="Ex: 01711223344">
                                </div>
                                <label for="">Password</label>
                                <div class="form-control">
                                    <input type="password" name="password" id="password" class="form-control" placeholder="*******">
                                </div>
                                {{-- <input type="button" value="Save" id="submitBtn"> --}}
                                <input type="submit" name="submit_btn" value="Save">
                        </form> 
                </div>
            </div>
        </div>
    </div>
</div>
    
@endsection



<script src="https://code.jquery.com/jquery-3.5.0.js"></script>
    

<script>

    // alert();

    let full_name = $('#full_name').val();
    let email_address = $('#email_address').val();
    let contact_number = $('#contact_number').val();
    let password = $('#password').val();
    let csrf_token = $('#csrf_token').val();


    let data = 'full_name='+full_name+'&email_address='+email_address+'&contact_number='+contact_number+'&password='+password+'&_token='+csrf_token;

    $('#submitBtn').click(function(){
        alert();
        // $.ajax({
        //     url: '{{ route("users.store") }}',
        //     type: 'POST',
        //     data: data,
        //     success:function(data){
        //         console.log(data);
        //     }
        // });
    });
        
</script>