@extends('layouts.app')

@section('content')
<div class="wrapper">
    <div class="row">
        <div class="col-md-12 content-justify-center">
            <div class="card">
                <div class="card-header">
                    Create New Customer
                </div>
                <div class="card-body">
                    @if($errors->any())
                    <ul>
                        @foreach($errors->all() as $error)
                        <li class="text-danger">
                            {{ $error }}
                        </li>
                        @endforeach
                    </ul>
                    @endif
                        <form action="{{ route('customers.store') }}" method="POST">

                                @csrf

                                <label for="">Name</label>
                                <div class="form-control">
                                    <input type="text" name="full_name" id="full_name" class="form-control" placeholder="Ex: Mr. X">
                                </div>
                                <label for="">Mobile Number</label>
                                <div class="form-control">
                                    <input type="number" maxlength="11" name="contact_number" id="contact_number" class="form-control" placeholder="Ex: 01711223344">
                                </div>
                                
                                <label for="">Address</label>
                                <div class="form-control">
                                    <textarea name="address" id="address" cols="30" rows="1"></textarea>
                                </div>
                                
                                <label for="">Photo</label>
                                <div class="form-control">
                                    <input type="file" name="attachment" id="attachment">
                                </div>
                                
                                <input type="submit" value="Save" id="submitBtn">
                        </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection