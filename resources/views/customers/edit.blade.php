@extends('layouts.app')

@section('content')
<div class="wrapper">
    <div class="row">
        <div class="col-md-12 content-justify-center">
            <div class="card">
                <div class="card-header">
                    Create New Customer
                </div>
                <div class="card-body">
                    @if($errors->any())
                    <ul>
                        @foreach($errors->all() as $error)
                        <li class="text-danger">
                            {{ $error }}
                        </li>
                        @endforeach
                    </ul>
                    @endif
                        <form action="{{ route('customers.update', $customer->id) }}" method="POST">

                                @csrf
                                @method("put")

                                {{-- <input type="hidden" name="id" value="{{ $customer->id }}"> --}}
                                <label for="">Name</label>
                                <div class="form-control">
                                    <input type="text" name="full_name" id="full_name" value="{{ $customer->name }}" class="form-control" placeholder="Ex: Mr. X">
                                </div>
                                <label for="">Mobile Number</label>
                                <div class="form-control">
                                    <input type="number" maxlength="11" name="contact_number" id="contact_number" value="{{ $customer->contact_number }}" class="form-control" placeholder="Ex: 01711223344">
                                </div>
                                
                                <label for="">Address</label>
                                <div class="form-control">
                                    <textarea name="address" id="address" cols="30" rows="1">{{ $customer->address }}</textarea>
                                </div>
                                
                                <label for="">Photo</label>
                                <div class="form-control">
                                    <img src="{{ $customer->attatchment }}" alt="" width="50" height="50">
                                    <input type="file" name="attatchment" id="attatchment">
                                </div>
                                
                                <input type="submit" value="Save" id="submitBtn">
                        </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection